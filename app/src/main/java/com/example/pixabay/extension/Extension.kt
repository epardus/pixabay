package com.example.pixabay.extension

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import androidx.annotation.LayoutRes
import androidx.annotation.StringRes
import androidx.core.content.getSystemService
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.pixabay.GlideApp


/**
 * Gets the required ViewModel defined by [T]
 * in the fragment.
 */
inline fun <reified T : ViewModel> Fragment.getViewModel(viewModelFactory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, viewModelFactory).get(T::class.java)
}

/**
 * Gets the required ViewModel defined by [T]
 * in the activity
 */
inline fun <reified T : ViewModel> FragmentActivity.getViewModel(viewModelFactory: ViewModelProvider.Factory): T {
    return ViewModelProviders.of(this, viewModelFactory).get(T::class.java)
}

/**
 * Inflates layout resource
 * @param layoutRes [Int] Layout resource ID
 */
fun ViewGroup.inflate(@LayoutRes layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}

/**
 * Loads image from url
 * @param url [String]
 */
fun ImageView.loadUrl(url: String) {
    GlideApp.with(context)
        .load(url)
        .into(this)
}

/**
 * Loads circular image from url
 * @param url [String]
 */
fun ImageView.loadCircularImage(url: String) {
    GlideApp.with(context)
        .load(url)
        .optionalCircleCrop()
        .placeholder(com.example.pixabay.R.drawable.ic_account_circle)
        .error(com.example.pixabay.R.drawable.ic_account_circle)
        .into(this)
}

/**
 * Loads image from url and use thumbnail as preview
 * @param thumbnailUrl [String]
 * @param url [String]
 */
fun ImageView.loadUrl(thumbnailUrl: String, url: String) {
    GlideApp.with(context)
        .load(url)
        .thumbnail(
            Glide.with(context)
                .load(thumbnailUrl)
        )
        .into(this)
}

/**
 * Replaces spaces between words with + sign whenever there are more than one
 * word. if the string is blank or has trailing spaces ,it will return empty String
 */
fun String.formatSearchQuery(): String {
    return trim().replace(" ", "+")
}

/**
 * Hides soft keyboard
 */
fun Activity.hideKeyboard() {
    val view = findViewById<View>(android.R.id.content)
    if (view != null) {
        val imm = getSystemService<InputMethodManager>()
        imm?.hideSoftInputFromWindow(view.windowToken, 0)
    }
}

/**
 * Gets String resource
 * @param res [Int] String resource ID
 */
fun RecyclerView.ViewHolder.getStringResource(@StringRes res: Int): String =
    itemView.context.getString(res)

/**
 * Gets String resource
 * @param res [Int] String resource ID
 * @param args
 */
fun RecyclerView.ViewHolder.getStringResource(@StringRes res: Int, vararg args: Any): String =
    itemView.context.getString(res, *args)