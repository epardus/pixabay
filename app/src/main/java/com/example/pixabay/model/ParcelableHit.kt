package com.example.pixabay.model

import android.os.Parcelable
import com.example.domain.model.Hit
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ParcelableHit(
    val comments: Int,
    val downloads: Int,
    val likes: Int,
    val user: String,
    val tags: String,
    val favorites: Int,
    val previewURL: String,
    val largeImageURL: String
) : Parcelable

fun Hit.toParcelableHit(): ParcelableHit {
    return ParcelableHit(
        comments = comments,
        downloads = downloads,
        likes = likes,
        user = user,
        tags = tags,
        favorites = favorites,
        previewURL = previewURL,
        largeImageURL = largeImageURL
    )
}