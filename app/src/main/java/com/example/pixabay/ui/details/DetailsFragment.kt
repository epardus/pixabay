package com.example.pixabay.ui.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.transition.TransitionInflater
import com.example.pixabay.R
import com.example.pixabay.extension.loadUrl
import com.google.android.material.chip.Chip
import kotlinx.android.synthetic.main.fragment_details.*

class DetailsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedElementEnterTransition =
                TransitionInflater.from(context).inflateTransition(android.R.transition.move)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_details, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val args = DetailsFragmentArgs.fromBundle(arguments)
        val hit = args.hitDetails
        val transitionName = getString(R.string.image_transition_name, args.imagePosition)

        largeImageView.transitionName = transitionName
        largeImageView.loadUrl(hit.previewURL, hit.largeImageURL)

        likesTextView.text = hit.likes.toString()

        favouritesTextView.text = hit.favorites.toString()
        commentTextView.text = hit.comments.toString()

        toolbar.setNavigationOnClickListener {
            findNavController().navigateUp()
        }

        toolbar.title = hit.user
        val tags = hit.tags.split(",")
        tags.forEach { tag ->
            val chip = Chip(context)
            chip.text = tag
            chipsGroup.addView(chip)
        }
    }
}
