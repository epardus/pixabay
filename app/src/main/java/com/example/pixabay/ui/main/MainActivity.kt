package com.example.pixabay.ui.main

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible

import androidx.navigation.findNavController
import androidx.navigation.ui.onNavDestinationSelected
import androidx.navigation.ui.setupWithNavController
import com.example.pixabay.R
import com.example.pixabay.di.ViewModelFactory
import com.example.pixabay.extension.formatSearchQuery
import com.example.pixabay.extension.getViewModel
import com.example.pixabay.extension.hideKeyboard
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.appbar_layout.*
import javax.inject.Inject

class MainActivity : DaggerAppCompatActivity(), SearchView.OnQueryTextListener {

    companion object {
        private const val QUERY_KEY = "QUERY_KEY"
    }

    private val navController by lazy { findNavController(R.id.nav_fragment) }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mainViewModel = getViewModel(viewModelFactory)

        val savedQuery = savedInstanceState?.getString(QUERY_KEY)
        val query = savedQuery ?: getString(R.string.default_search_query)

        bottomNavigationView.setupWithNavController(navController)
        searchView.setOnQueryTextListener(this)
        searchView.setQuery(query, savedQuery == null)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return item.onNavDestinationSelected(navController)
                || super.onOptionsItemSelected(item)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(QUERY_KEY, searchView.query.toString())
        super.onSaveInstanceState(outState)
    }

    /**
     * Callbacks from [SearchView.OnQueryTextListener]
     */
    override fun onQueryTextSubmit(query: String): Boolean {
        searchImages(query)
        hideKeyboard()
        return true
    }

    override fun onQueryTextChange(newText: String): Boolean {
        return false
    }

    private fun searchImages(query: String) {
        if (query.formatSearchQuery().isNotEmpty()) {
            mainViewModel.searchImages(query)
        } else {
            Toast.makeText(this, getString(R.string.enter_keyword_message), Toast.LENGTH_LONG)
                .show()
        }
    }
}