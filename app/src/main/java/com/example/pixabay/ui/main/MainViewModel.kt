package com.example.pixabay.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.model.Hit
import com.example.domain.model.Resource
import com.example.domain.model.VideoHit
import com.example.domain.usecase.SearchImagesUseCase
import com.example.domain.usecase.SearchVideosUseCase
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class MainViewModel @Inject constructor(
    private val searchImagesUseCase: SearchImagesUseCase,
    private val searchVidoesUseCase: SearchVideosUseCase
) : ViewModel() {

    private val compositeDisposable = CompositeDisposable()
    private val imagesResultLD = MutableLiveData<Resource<List<Hit>>>()
    private val videosResultLD = MutableLiveData<Resource<List<VideoHit>>>()

    fun imagesResult() = imagesResultLD

    fun videosResult() = videosResultLD

    fun searchImages(query: String) {
        val disposable = searchImagesUseCase.execute(query)
            .doOnSubscribe {
                val data = imagesResultLD.value?.data.orEmpty()
                imagesResultLD.setValue(Resource.loading(data))
            }
            .subscribe(
                { hits ->
                    imagesResultLD.setValue(Resource.success(hits))
                },
                { throwable ->
                    imagesResultLD.setValue(Resource.error(throwable.localizedMessage, listOf()))
                }
            )
        compositeDisposable.add(disposable)
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }
}