package com.example.pixabay.ui.main

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.model.Hit
import com.example.pixabay.R
import com.example.pixabay.extension.getStringResource
import com.example.pixabay.extension.inflate
import com.example.pixabay.extension.loadUrl
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_hit_list.*
import java.lang.ref.WeakReference

class HitsAdapter(hitDiff: HitDiff) : ListAdapter<Hit, HitViewHolder>(hitDiff) {

    private lateinit var onHitItemClickListenerReference: WeakReference<OnHitItemClickListener>

    constructor (onHitItemClickListener: OnHitItemClickListener) : this(HitDiff()) {
        onHitItemClickListenerReference = WeakReference(onHitItemClickListener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HitViewHolder {
        return HitViewHolder(
            parent.inflate(R.layout.item_hit_list),
            onHitItemClickListenerReference
        )
    }

    override fun onBindViewHolder(holder: HitViewHolder, position: Int) {
        holder.bind(getItem(holder.adapterPosition))
    }
}

class HitViewHolder(
    override val containerView: View,
    private val onHitItemClickListenerReference: WeakReference<OnHitItemClickListener>
) : RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(hit: Hit) {
        imageView.loadUrl(hit.previewURL)
        itemView.setOnClickListener {
            val transitionName = getStringResource(R.string.image_transition_name, adapterPosition)
            imageView.transitionName = transitionName
            onHitItemClickListenerReference.get()?.onHitItemClick(hit, imageView, adapterPosition)
        }
    }
}

class HitDiff : DiffUtil.ItemCallback<Hit>() {
    override fun areContentsTheSame(oldItem: Hit, newItem: Hit): Boolean = oldItem == newItem

    override fun areItemsTheSame(oldItem: Hit, newItem: Hit): Boolean = oldItem == newItem
}

/**
 * Callback for Hit list item click
 */
interface OnHitItemClickListener {
    /**
     * On hit item click callback
     * @param hit [Hit]
     * @param preview [View] view to be animated using shared transition
     * @param position [Int]
     */
    fun onHitItemClick(hit: Hit, preview: View, position: Int)
}