package com.example.pixabay.ui.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Observer
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.example.domain.model.Hit
import com.example.domain.model.Resource
import com.example.pixabay.R
import com.example.pixabay.di.DaggerSupportFragment
import com.example.pixabay.di.ViewModelFactory
import com.example.pixabay.extension.getViewModel
import com.example.pixabay.model.toParcelableHit
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_images.*
import javax.inject.Inject

class ImagesFragment : DaggerSupportFragment(),
    OnHitItemClickListener {

    companion object {
        private const val SPAN_SIZE = 1
    }

    @Inject
    lateinit var viewModelFactory: ViewModelFactory
    private lateinit var mainViewModel: MainViewModel
    private val hitsAdapter = HitsAdapter(onHitItemClickListener = this)

    override fun onAttach(context: Context) {
        super.onAttach(context)
        val activity = context as FragmentActivity
        mainViewModel = activity.getViewModel(viewModelFactory)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_images, container, false)


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val gridLayoutManager =
            GridLayoutManager(context, resources.getInteger(R.integer.columns_count))
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int) = SPAN_SIZE
        }
        hitsRecyclerView.adapter = hitsAdapter
        hitsRecyclerView.layoutManager = gridLayoutManager
        mainViewModel.imagesResult()
            .observe(viewLifecycleOwner, Observer { it?.apply { handleResult(this) } })
    }

    /**
     * Callback for list item clicks
     */
    override fun onHitItemClick(hit: Hit, preview: View, position: Int) {
        val parcelableHit = hit.toParcelableHit()
        val extras = FragmentNavigatorExtras(preview to preview.transitionName)
        val action = ImagesFragmentDirections.actionDetailsDest(parcelableHit, position)
        findNavController().navigate(action, extras)
    }

    private fun handleResult(resource: Resource<List<Hit>>) {
        when (resource.status) {
            Resource.Status.SUCCESS -> {
                progressBar.isVisible = false
                emptyView.isVisible = resource.data.isEmpty()
                hitsAdapter.submitList(resource.data)
            }

            Resource.Status.ERROR -> {
                progressBar.isVisible = false
                val message = resource.message
                    ?: getString(com.example.pixabay.R.string.general_error_message)
                Snackbar.make(hitsRecyclerView, message, Snackbar.LENGTH_LONG).show()
            }

            Resource.Status.LOADING -> progressBar.isVisible = true
        }
    }
}