package com.example.pixabay

import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule

@GlideModule
class PixabayGlideModule : AppGlideModule()