package com.example.pixabay.di

import android.app.Application
import android.content.Context
import com.example.data.api.ParamsInterceptor
import com.example.data.api.PixabayApiService
import com.example.data.base.AndroidAppSchedulers
import com.example.data.repository.PixabayImagesRepository
import com.example.data.repository.PixabayVideosRepository
import com.example.domain.base.AppSchedulers
import com.example.domain.repository.ImagesRepository
import com.example.domain.repository.VideosRepository
import com.example.pixabay.R
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.osrameinstone.configuration.v2.di.ViewModelModule
import dagger.Binds
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
internal abstract class AppModule {

    @Binds
    abstract fun appSchedulers(appSchedulers: AndroidAppSchedulers): AppSchedulers

    @Binds
    abstract fun imagesRepository(imagesRepository: PixabayImagesRepository): ImagesRepository

    @Binds
    abstract fun videosRepository(videosRepository: PixabayVideosRepository): VideosRepository

    @Module
    companion object {

        @JvmStatic
        @Provides
        @Singleton
        fun provideContext(application: Application): Context {
            return application
        }

        @JvmStatic
        @Provides
        @Named("baseUrl")
        fun provideBaseUrl(context: Context): String = context.getString(R.string.base_url)


        @JvmStatic
        @Provides
        @Named("cacheSize")
        fun provideCacheSize(): Long = 5 * 1024 * 1024 // 5 MB

        @JvmStatic
        @Provides
        @Singleton
        fun provideGson(): Gson {
            return GsonBuilder()
                .create()
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideCache(
            context: Context,
            @Named("cacheSize") cacheSize: Long
        ): Cache {
            return Cache(context.cacheDir, cacheSize)
        }

        @JvmStatic
        @Provides
        @Singleton
        fun provideOkHttpClient(paramsInterceptor: ParamsInterceptor, cache: Cache): OkHttpClient {
            // Client builder
            val httpClient = OkHttpClient.Builder()
            // Logging interceptor
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClient.addInterceptor(loggingInterceptor)
            // Common parameters interceptor
            httpClient.addInterceptor(paramsInterceptor)
            // Cache
            httpClient.cache(cache)
            return httpClient.build()
        }

        @JvmStatic
        @Provides
        @Singleton
        fun providePixabayApiService(
            @Named("baseUrl") baseUrl: String,
            gson: Gson,
            httpClient: OkHttpClient
        ): PixabayApiService {

            // API service
            return Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(httpClient)
                .build().create<PixabayApiService>(PixabayApiService::class.java)
        }
    }
}
