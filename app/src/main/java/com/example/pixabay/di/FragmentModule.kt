package com.example.pixabay.di


import com.example.pixabay.ui.main.ImagesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {
    @ContributesAndroidInjector
    internal abstract fun contributeImagesFragment(): ImagesFragment
}
