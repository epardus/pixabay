package com.example.pixabay


import com.example.pixabay.di.DaggerAppComponent
import dagger.android.support.DaggerApplication
import timber.log.Timber

class PixabayApp : DaggerApplication() {
    override fun applicationInjector() = DaggerAppComponent.builder().application(this).build()

    override fun onCreate() {
        super.onCreate()
        applicationInjector().inject(this)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}