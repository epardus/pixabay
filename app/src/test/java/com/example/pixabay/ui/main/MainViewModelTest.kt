package com.example.pixabay.ui.main

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.domain.model.Hit
import com.example.domain.repository.ImagesRepository
import com.example.domain.repository.VideosRepository
import com.example.domain.usecase.SearchImagesUseCase
import com.example.domain.usecase.SearchVideosUseCase
import com.example.pixabay.common.TestSchedulers
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test


class MainViewModelTest {

    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()
    private var appSchedulers = TestSchedulers()

    private var imagesRepository = mock<ImagesRepository>()
    private var videosRepository = mock<VideosRepository>()
    private lateinit var viewModel: MainViewModel

    private val data = listOf(
        Hit(
            comments = 10,
            downloads = 10,
            favorites = 10,
            likes = 10,
            largeImageURL = "",
            previewURL = "",
            user = "me",
            tags = "flowers, blue",
            userImage = "some url"
        )
    )

    @Before
    fun setUp() {
        val searchImagesUseCase = SearchImagesUseCase(appSchedulers, imagesRepository)
        val searchVideosUseCase = SearchVideosUseCase(appSchedulers, videosRepository)

        viewModel = MainViewModel(searchImagesUseCase, searchVideosUseCase)
    }

    @Test
    fun searchImages() {
        val query = "flower"
        whenever(imagesRepository.getImages(query))
            .thenReturn(Single.just(data))

        viewModel.searchImages(query)
        Assert.assertEquals(data, viewModel.imagesResult().value?.data)
    }

    @Test
    fun searchImagesError() {
        val query = ""
        val response = Throwable("Error response")
        whenever(imagesRepository.getImages(query))
            .thenReturn(Single.error(response))

        viewModel.searchImages(query)
        Assert.assertEquals(response.localizedMessage, viewModel.imagesResult().value?.message)
    }
}