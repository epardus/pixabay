package com.example.pixabay.extension

import org.junit.Assert
import org.junit.Test

import org.junit.Assert.*

class ExtensionTest {

    @Test
    fun formatSearchQuery() {
        Assert.assertEquals("", "       ".formatSearchQuery())
        Assert.assertEquals("", "".formatSearchQuery())
        Assert.assertEquals("flowers", " flowers ".formatSearchQuery())
        Assert.assertEquals("yellow+flowers", " yellow flowers ".formatSearchQuery())
    }
}