package com.example.domain.model

data class Video(
    val url: String,
    val width: Int,
    val size: Int,
    val height: Int
)