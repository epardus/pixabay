package com.example.domain.model

data class Videos(
    val large: Video,
    val medium: Video,
    val small: Video,
    val tiny: Video
)