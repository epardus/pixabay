package com.example.domain.model

data class Hit(
    val comments: Int,
    val downloads: Int,
    val likes: Int,
    val user: String,
    val tags: String,
    val favorites: Int,
    val previewURL: String,
    val largeImageURL: String,
    val userImage: String
)