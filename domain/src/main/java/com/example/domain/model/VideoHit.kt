package com.example.domain.model

data class VideoHit(
    val videos: Videos,
    val tags: String,
    val downloads: Int,
    val likes: Int,
    val favorites: Int,
    val duration: Int,
    val id: Int,
    val userId: Int,
    val views: Int,
    val comments: Int,
    val userImageURL: String,
    val pageURL: String,
    val type: String,
    val user: String
)