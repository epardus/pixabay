package com.example.domain.repository

import com.example.domain.model.VideoHit
import io.reactivex.Observable
import io.reactivex.Single

interface VideosRepository {
    /**
     * Search for videos with a required query
     * @param query [String]
     */
    fun getVideos(query: String): Single<List<VideoHit>>
}