package com.example.domain.repository

import com.example.domain.model.Hit
import io.reactivex.Observable
import io.reactivex.Single

interface ImagesRepository {
    /**
     * Search for images with a required query
     * @param query [String]
     */
    fun getImages(query: String): Single<List<Hit>>
}