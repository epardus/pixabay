package com.example.domain.usecase

import com.example.domain.base.AppSchedulers
import com.example.domain.model.Hit
import com.example.domain.repository.ImagesRepository
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class SearchImagesUseCase @Inject constructor(
    appSchedulers: AppSchedulers,
    private val imagesRepository: ImagesRepository
) : UseCase<String, List<Hit>>(appSchedulers) {
    override fun createUseCaseObservable(param: String): Single<List<Hit>> {
        return imagesRepository.getImages(param)
    }
}