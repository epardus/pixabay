package com.example.domain.usecase

import com.example.domain.base.AppSchedulers
import com.example.domain.model.VideoHit
import com.example.domain.repository.VideosRepository
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class SearchVideosUseCase @Inject constructor(
    appSchedulers: AppSchedulers,
    private val videosRepository: VideosRepository
) : UseCase<String, List<VideoHit>>(appSchedulers) {
    override fun createUseCaseObservable(param: String): Single<List<VideoHit>> {
        return videosRepository.getVideos(param)
    }
}