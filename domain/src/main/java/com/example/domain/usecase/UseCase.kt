package com.example.domain.usecase

import com.example.domain.base.AppSchedulers
import io.reactivex.Observable
import io.reactivex.Single

abstract class UseCase<in P, R>(private val appSchedulers: AppSchedulers) {

    fun execute(param: P): Single<R> {
        return createUseCaseObservable(param).compose(appSchedulers.singleTransformer())
    }

    protected abstract fun createUseCaseObservable(param: P): Single<R>
}