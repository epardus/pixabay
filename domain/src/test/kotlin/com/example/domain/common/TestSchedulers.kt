package com.example.domain.common

import com.example.domain.base.AppSchedulers
import io.reactivex.CompletableTransformer
import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer
import io.reactivex.schedulers.Schedulers

class TestSchedulers : AppSchedulers {
    override fun completableTransformer(): CompletableTransformer {
        return CompletableTransformer { it ->
            it.subscribeOn(Schedulers.trampoline()).observeOn(Schedulers.trampoline())
        }
    }

    override fun <T> singleTransformer(): SingleTransformer<T, T> {
        return SingleTransformer { it ->
            it.subscribeOn(Schedulers.trampoline()).observeOn(Schedulers.trampoline())
        }
    }

    override fun <T> observableTransformer(): ObservableTransformer<T, T> {
        return ObservableTransformer { it ->
            it.subscribeOn(Schedulers.trampoline()).observeOn(Schedulers.trampoline())
        }
    }
}