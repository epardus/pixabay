package com.example.domain.usecase

import com.example.domain.common.TestSchedulers
import com.example.domain.model.Hit
import com.example.domain.repository.ImagesRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Test

import org.junit.Assert.*
import org.junit.Rule

class SearchImagesUseCaseTest {

    private var appSchedulers = TestSchedulers()
    private var imagesRepository = mock<ImagesRepository>()
    private lateinit var searchImagesUseCase: SearchImagesUseCase

    private val data = listOf(
        Hit(
            comments = 10,
            downloads = 10,
            favorites = 10,
            likes = 10,
            largeImageURL = "",
            previewURL = "",
            user = "me",
            tags = "flowers, blue",
            userImage = "image"
        )
    )

    @Before
    fun setUp() {
        searchImagesUseCase = SearchImagesUseCase(appSchedulers, imagesRepository)
    }

    @Test
    fun searchImagesByQuery() {
        val query = "flower"
        whenever(imagesRepository.getImages(query))
            .thenReturn(Single.just(data))

        searchImagesUseCase.execute(query).test().assertValue(data)
    }

    @Test
    fun searchImagesError() {
        val query = ""
        val error = Throwable("Error response")
        whenever(imagesRepository.getImages(query))
            .thenReturn(Single.error(error))

        searchImagesUseCase.execute(query).test().assertError(error)

    }
}