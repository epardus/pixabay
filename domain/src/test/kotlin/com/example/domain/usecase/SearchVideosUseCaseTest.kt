package com.example.domain.usecase

import com.example.domain.common.TestSchedulers
import com.example.domain.model.Video
import com.example.domain.model.VideoHit
import com.example.domain.model.Videos
import com.example.domain.repository.VideosRepository
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Single
import org.junit.Test

import org.junit.Before

class SearchVideosUseCaseTest {
    private var appSchedulers = TestSchedulers()
    private var videosRepository = mock<VideosRepository>()
    private lateinit var searchVideosUseCase: SearchVideosUseCase

    private val data = listOf(
        VideoHit(
            comments = 10,
            downloads = 10,
            favorites = 10,
            likes = 10,
            user = "me",
            tags = "flowers, blue",
            videos = Videos(
                large = Video(url = "large url", width = 10, height = 10, size = 10),
                medium = Video(url = "medium url", width = 10, height = 10, size = 10),
                small = Video(url = "small url", width = 10, height = 10, size = 10),
                tiny = Video(url = "tiny url", width = 10, height = 10, size = 10)
            ),
            userId = 1,
            duration = 10,
            userImageURL = "user image url",
            type = "Video",
            pageURL = "page url",
            views = 9,
            id = 11
        )
    )

    @Before
    fun setUp() {
        searchVideosUseCase = SearchVideosUseCase(appSchedulers, videosRepository)
    }

    @Test
    fun searchVideosByQuery() {
        val query = "flower"
        whenever(videosRepository.getVideos(query))
            .thenReturn(Single.just(data))

        searchVideosUseCase.execute(query).test().assertValue(data)
    }

    @Test
    fun searchVideosError() {
        val query = ""
        val error = Throwable("Error response")
        whenever(videosRepository.getVideos(query))
            .thenReturn(Single.error(error))

        searchVideosUseCase.execute(query).test().assertError(error)

    }
}