package com.example.data.model

import com.example.domain.model.Video
import com.example.domain.model.VideoHit
import com.example.domain.model.Videos
import org.junit.Assert
import org.junit.Test

class VideoHitDataTest {

    private val videoHitData = VideoHitData(
        videos = VideosData(
            large = VideoData(
                url = "https://player.vimeo.com/external/161687568.hd.mp4?s=eff398051a1dd6ceaa16b85538940b07cebede70&profile_id=119",
                width = 1920,
                size = 13392449,
                height = 1080
            ),
            small = VideoData(
                url = "https://player.vimeo.com/external/161687568.sd.mp4?s=4feca1b7112229c1ef975f13575a65a0bfe93767&profile_id=165",
                width = 960,
                size = 4940642,
                height = 540
            ),
            medium = VideoData(
                url = "https://player.vimeo.com/external/161687568.hd.mp4?s=eff398051a1dd6ceaa16b85538940b07cebede70&profile_id=174",
                width = 1280,
                size = 7770061,
                height = 720
            ),
            tiny = VideoData(
                url = "https://player.vimeo.com/external/161687568.sd.mp4?s=4feca1b7112229c1ef975f13575a65a0bfe93767&profile_id=164",
                width = 640,
                size = 1769296,
                height = 360
            )
        ),
        tags = "dandelion, flowers, yellow",
        downloads = 9319,
        likes = 90,
        favorites = 55,
        duration = 23,
        id = 2719,
        user_id = 1981326,
        views = 21313,
        comments = 24,
        userImageURL = "https://cdn.pixabay.com/user/2016/05/06/18-47-35-699_250x250.png",
        pageURL = "https://pixabay.com/videos/id-2719/",
        type = "film",
        user = "FMFA"
    )

    private val videoHit = VideoHit(
        videos = Videos(
            large = Video(
                url = "https://player.vimeo.com/external/161687568.hd.mp4?s=eff398051a1dd6ceaa16b85538940b07cebede70&profile_id=119",
                width = 1920,
                size = 13392449,
                height = 1080
            ),
            small = Video(
                url = "https://player.vimeo.com/external/161687568.sd.mp4?s=4feca1b7112229c1ef975f13575a65a0bfe93767&profile_id=165",
                width = 960,
                size = 4940642,
                height = 540
            ),
            medium = Video(
                url = "https://player.vimeo.com/external/161687568.hd.mp4?s=eff398051a1dd6ceaa16b85538940b07cebede70&profile_id=174",
                width = 1280,
                size = 7770061,
                height = 720
            ),
            tiny = Video(
                url = "https://player.vimeo.com/external/161687568.sd.mp4?s=4feca1b7112229c1ef975f13575a65a0bfe93767&profile_id=164",
                width = 640,
                size = 1769296,
                height = 360
            )
        ),
        tags = "dandelion, flowers, yellow",
        downloads = 9319,
        likes = 90,
        favorites = 55,
        duration = 23,
        id = 2719,
        userId = 1981326,
        views = 21313,
        comments = 24,
        userImageURL = "https://cdn.pixabay.com/user/2016/05/06/18-47-35-699_250x250.png",
        pageURL = "https://pixabay.com/videos/id-2719/",
        type = "film",
        user = "FMFA"
    )

    @Test
    fun map() {
        Assert.assertEquals(videoHit, videoHitData.map())
    }
}