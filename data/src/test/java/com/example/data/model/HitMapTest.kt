package com.example.data.model

import com.example.domain.model.Hit
import org.junit.Assert
import org.junit.Test

class HitMapTest {

    private val hitData = HitData(
        largeImageURL = "https://pixabay.com/get/ea35b70c2afc053ed1584d05fb1d4797e377e0d010b20c4090f4c571a2e4b2bbdd_1280.jpg",
        webformatHeight = 426,
        webformatWidth = 640,
        likes = 849,
        imageWidth = 6000,
        id = 3063284,
        user_id = 1564471,
        views = 477662,
        comments = 191,
        pageURL = "https://pixabay.com/en/rose-flower-petal-floral-noble-3063284/",
        imageHeight = 4000,
        webformatURL = "https://pixabay.com/get/ea35b70c2afc053ed1584d05fb1d4797e377e0d010b20c4090f4c571a2e4b2bbdd_640.jpg",
        type = "photo",
        previewHeight = 99,
        tags = "rose, flower, petal",
        downloads = 289456,
        user = "annca",
        favorites = 720,
        imageSize = 3574625,
        previewWidth = 150,
        userImageURL = "https://cdn.pixabay.com/user/2015/11/27/06-58-54-609_250x250.jpg",
        previewURL = "https://cdn.pixabay.com/photo/2018/01/05/16/24/rose-3063284_150.jpg"
    )

    private val hit = Hit(
        comments = 191,
        downloads = 289456,
        likes = 849,
        user = "annca",
        tags = "rose, flower, petal",
        favorites = 720,
        largeImageURL = "https://pixabay.com/get/ea35b70c2afc053ed1584d05fb1d4797e377e0d010b20c4090f4c571a2e4b2bbdd_1280.jpg",
        previewURL = "https://cdn.pixabay.com/photo/2018/01/05/16/24/rose-3063284_150.jpg",
        userImage = "https://cdn.pixabay.com/user/2015/11/27/06-58-54-609_250x250.jpg"
    )

    @Test
    fun map() {
        Assert.assertEquals(hit, hitData.map())
    }
}