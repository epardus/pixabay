package com.example.data.mapper

/**
 * Maps the implementer of this interface  to [T]
 */
interface Mapper<T> {
    fun map(): T
}