package com.example.data.model

data class ImagesResponse(
    val hits: List<HitData>,
    val total: Int,
    val totalHits: Int
)