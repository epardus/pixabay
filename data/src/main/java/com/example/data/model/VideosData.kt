package com.example.data.model

import com.example.data.mapper.Mapper
import com.example.domain.model.Videos

data class VideosData(
    val large: VideoData,
    val medium: VideoData,
    val small: VideoData,
    val tiny: VideoData
) : Mapper<Videos> {
    override fun map(): Videos {
        return Videos(
            large = large.map(),
            medium = medium.map(),
            small = small.map(),
            tiny = tiny.map()
        )
    }
}