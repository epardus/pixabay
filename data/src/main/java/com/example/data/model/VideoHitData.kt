package com.example.data.model

import com.example.data.mapper.Mapper
import com.example.domain.model.VideoHit

data class VideoHitData(
    val videos: VideosData,
    val tags: String,
    val downloads: Int,
    val likes: Int,
    val favorites: Int,
    val duration: Int,
    val id: Int,
    val user_id: Int,
    val views: Int,
    val comments: Int,
    val userImageURL: String,
    val pageURL: String,
    val type: String,
    val user: String
) : Mapper<VideoHit?> {
    override fun map(): VideoHit? {
        return VideoHit(
            comments = comments,
            downloads = downloads,
            favorites = favorites,
            likes = likes,
            user = user,
            userId = user_id,
            videos = videos.map(),
            views = views,
            duration = duration,
            id = id,
            pageURL = pageURL,
            tags = tags,
            type = type,
            userImageURL = userImageURL
        )
    }
}