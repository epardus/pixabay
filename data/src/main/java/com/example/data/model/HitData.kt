package com.example.data.model

import com.example.data.mapper.Mapper
import com.example.domain.model.Hit

data class HitData(
    val comments: Int,
    val downloads: Int,
    val favorites: Int,
    val id: Int,
    val imageHeight: Int,
    val imageSize: Int,
    val imageWidth: Int,
    val largeImageURL: String,
    val likes: Int,
    val pageURL: String,
    val previewHeight: Int,
    val previewURL: String,
    val previewWidth: Int,
    val tags: String,
    val type: String,
    val user: String,
    val userImageURL: String,
    val user_id: Int,
    val views: Int,
    val webformatHeight: Int,
    val webformatURL: String,
    val webformatWidth: Int
) : Mapper<Hit?> {
    override fun map(): Hit? {
        return Hit(
            comments = comments,
            downloads = downloads,
            favorites = favorites,
            likes = likes,
            user = user,
            largeImageURL = largeImageURL,
            previewURL = previewURL,
            tags = tags,
            userImage = userImageURL
        )
    }
}