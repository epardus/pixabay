package com.example.data.model

data class VideosResponse(
    val hits: List<VideoHitData>,
    val total: Int,
    val totalHits: Int
)