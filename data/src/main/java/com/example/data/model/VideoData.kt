package com.example.data.model

import com.example.data.mapper.Mapper
import com.example.domain.model.Video

data class VideoData(
    val url: String,
    val width: Int,
    val size: Int,
    val height: Int
) : Mapper<Video> {
    override fun map(): Video {
        return Video(
            url = url,
            width = width,
            size = size,
            height = height
        )
    }
}