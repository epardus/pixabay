package com.example.data.api

import com.example.data.model.ImagesResponse
import com.example.data.model.VideosResponse
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface PixabayApiService {

    @GET("api/")
    fun getImages(@Query("q") query: String): Single<ImagesResponse>

    @GET("api/videos")
    fun getVideos(@Query("q") query: String): Single<VideosResponse>
}