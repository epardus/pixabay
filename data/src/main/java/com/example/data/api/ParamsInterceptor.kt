package com.example.data.api

import android.content.Context
import com.example.data.R
import okhttp3.Interceptor
import okhttp3.Response
import javax.inject.Inject

/**
 * Adds common params to every call
 */
class ParamsInterceptor @Inject constructor(private val context: Context) : Interceptor {
    companion object {
        private const val API_KEY = "key"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val original = chain.request()
        val originalHttpUrl = original.url()

        val url = originalHttpUrl.newBuilder()
            .addQueryParameter(API_KEY, context.getString(R.string.api_key))
            .build()

        val requestBuilder = original.newBuilder().url(url)
        val request = requestBuilder.build()
        return chain.proceed(request)
    }
}