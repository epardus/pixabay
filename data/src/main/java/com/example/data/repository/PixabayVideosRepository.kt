package com.example.data.repository

import com.example.data.api.PixabayApiService
import com.example.data.model.VideosResponse
import com.example.domain.model.VideoHit
import com.example.domain.repository.VideosRepository
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class PixabayVideosRepository @Inject constructor(private val pixabayApiService: PixabayApiService) : VideosRepository {
    override fun getVideos(query: String): Single<List<VideoHit>> {
        return pixabayApiService.getVideos(query).map { mapResponse(it) }
    }

    private fun mapResponse(videosResponse: VideosResponse): List<VideoHit> {
        return videosResponse.hits.mapNotNull { it.map() }
    }
}