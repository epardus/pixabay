package com.example.data.repository

import com.example.data.api.PixabayApiService
import com.example.data.model.ImagesResponse
import com.example.domain.model.Hit
import com.example.domain.repository.ImagesRepository
import io.reactivex.Observable
import io.reactivex.Single
import javax.inject.Inject

class PixabayImagesRepository @Inject constructor(private val pixabayApiService: PixabayApiService) : ImagesRepository {
    override fun getImages(query: String): Single<List<Hit>> {
        return pixabayApiService.getImages(query).map { mapResponse(it) }
    }

    private fun mapResponse(imagesResponse: ImagesResponse): List<Hit> {
        return imagesResponse.hits.mapNotNull { it.map() }
    }
}