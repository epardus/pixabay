# Pixabay

Architecture:
Clean Architecture + MVVM design pattern

Libraries:
- Android architecture components (ViewModel + LiveData)
- RxJava RxAndroid
- Dagger 2
- Android Navigation component
- Gson
- Retrofit
- Glide
- AndroidX libraries

Features

● The user can use pixabay api [https://pixabay.com/api/docs/#api_search_images]
 to  be able to search for images entering one or more words in a text field
 
● User can see a set of results. Each entry should show:

 ○ A thumbnail of the image
 
● The app caches the response
 
● The detail screen contains:

 ○ A bigger version of the image
 
 ○ The name of the user (photographer)
 
 ○ A list of image’s tags
 
 ○ The number of likes
 
 ○ The number of favourites
 
 ○ The number of comments
 
 
● When the app starts a search for the string “flowers” will be triggered


#Android studio version:
In order to be able to run the source code. you should install Android studio with the following version or above:

Android Studio 3.4 Canary 10

Build #AI-183.4588.61.34.5202479, built on December 28, 2018

JRE: 1.8.0_152-release-1248-b22 x86_64

JVM: OpenJDK 64-Bit Server VM by JetBrains s.r.o

macOS 10.14.3


#Test Device
Android emulator for Pixel 2XL API 28

#CI Environment
Bitrise
